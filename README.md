# Lodr on Prem Deployment
![Lodr Architecture](lodr_arch.png)

Users can connect to a specific S3 bucket or upload files directly into designated user uploads bucket.  All user uploaded files are stored with this key format:
```
<user_id>/uploads/<filename>
```

The frontend application is a Ruby on Rails application running Ruby 2.3.1 on the Puma application server.  You can front the application server with Nginx (recommended) or Apache if needed.  While the frontend captures user input, job metadata, and job statistics, the Job manager actually executes the data load process.

The Job manager gets an incoming request, submits the required lambda jobs, and finally executes the load.  It also handles unzipping files, sampling data, and profiling data.  The disk space provisioned here should consider the largest uncompressed file size you wish to support.

## S3 User Uploads Bucket
1. Create a bucket in the appropriate region, perhaps named <company>-lodr-user-uploads
2. Create a dedicated user with read/write access just to this bucket.  Let’s call this user s3_uploader
3. Attach the Amazon managed policy AmazonS3FullAccess to this user
4. Create a custom policy to provide access to the specific bucket resource:

```javascript
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": [
                "arn:aws:s3:::<bucket_name>/*"
            ]
        }
    ]
}
```

5. Lodr sends user uploads directly to the uploads bucket with a signed key and requires a valid CORS policy be attached to the bucket
```xml
<?xml version="1.0" encoding="UTF-8"?>
<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
<CORSRule>
    <AllowedOrigin>https://*.<server domain></AllowedOrigin>
    <AllowedMethod>POST</AllowedMethod>
    <AllowedMethod>PUT</AllowedMethod>
    <AllowedHeader>*</AllowedHeader>
</CORSRule>
</CORSConfiguration>
```
** Update the AllowedOrigin to reflect the domain/ip address of the instance running the Lodr App (Ruby on Rails)

## RDS Instance
The Lodr App (Ruby on Rails) stores data in an Postgres RDS instance.  This includes user authentication, loader configurations, sample data, and background job processing information.

We recommend at least a db.t2.large instance with the PostgreSQL 9.5.4 engine.
* Postgres Version 9.5.4
* Storage: General Purpose SSD (50gb to start)

Create a database named **lodr_prod** and create a user with both read and write access to this db.

## EC2 Instance Setup
You can run the Lodr App and Lodr Job Manager on the same EC2 instance or run each on a separate instance.  If you run both on the same machine an m3.medium should be sufficient for most deployments.

### Instance Package Configuration
1. yum update
2. sudo yum groupinstall "Development Tools"
3. sudo yum install postgresql95-devel
4. sudo yum install -y openssl-devel readline-devel zlib-devel
5. sudo yum install curl git-core nginx -y

### Install Ruby/Rails Packages
1. git clone https://github.com/rbenv/rbenv.git ~/.rbenv
2. cd ~/.rbenv && src/configure && make -C src [fails its normal]
3. Edit ~/.bash_profile by running these commands
  - echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
  - echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
4. Install ruby-build:
  - git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
5. rbenv install -l [should list all available ruby version, may have to restart shell]
6. rbenv install 2.3.1 [takes a while]
7. cd ~/.rbenv/plugins
8. git clone https://github.com/sstephenson/rbenv-vars.git
9. You might have to restart the shell here
10. rbenv global 2.3.1
11. ruby -v [should print 2.3.1]

### Installing Java
1. Download JDK rpm package from Oracle web site and upload it to your EC2 instance
2. Run “sudo rpm -i jdk-8u77-linux-x64.rpm”
3. Check both java and javac to make sure that it is pointing to the ones under /usr/java/jdk1.8.0_77/
4. If this is not the case, following the steps below:

  - If not, then let's create one more alternative for Java for Sun JDK
sudo /usr/sbin/alternatives --install /usr/bin/java java /usr/java/jdk1.8.0_77/bin/java 20000

  - Set the SUN JDK as the default java
    sudo /usr/sbin/alternatives --config java

## DynamoDB
There are four tables in DynamoDB for Lodr:

1. CanceledJob - Used to notify all Lambda functions when user cancel a running job
2. CanceledQuery - Used to notify query executor to cancel a running SQL query
3. JobStatus - Used to coordinate among all running Lambda functions for a job
4. ColumnInfo - This is a support table for JobStatus and used to store all column information discovered by TypeAnalyzer. There is a one to many relationship from JobStatus table to ColumnInfo table.

### Setup Tables in DynamoDB Console
Login to DynamoDB console and create each table with the following information


Table Name | Partition Key | Sort Key | Read Capacity | Write Capacity
---------- | ------------- | -------- | ------------- | ------------ |
CanceledJob | jobId(String) | | 5 | 2
CanceledQuery | queryId(String) | | 5 | 2
ColumnInfo | jobId(String) | batchId(Number) | 100 | 200
JobStatus | jobId(String) | batchId(NUmber) | 100 | 250

Read/Write capacity for ColumnInfo and JobStatus tables are really dependent on load. If load is evenly distributed during the data, Read/Write capacity could be safely lowered to 50%.

**You can change table names in the application.conf file located in the config directory. See more below.**

## Lambda Functions
Lodr uses a series of Lambda functions to analyze and transform incoming data.

1. **SampleSourceFunction** - Used to sample a file and determine encoding, delimiter, quote character, etc.
2. **SampleWranglerSourceFunction** - Used to read in sample files for wrangler (first 2000 rows by default)
3. **ProcessDataFunction** - Used to read and process 200MB of data from S3
4. **ProcessDataInMemoryFunction** - Same as ProcessDataFunction, but it process entire file at once.
5. **CleanDataFunction** - This is a helper function used to clean up all DynamoDB tables each night. Trigger by CloudWatch event each night.

### Setting up Lambda Functions in the AWS Console

First of all, create a role in IAM with AWSLambdaFullAccess and AmazonS3FullAccess policies and then using the following information to create Lambda functions in Lambda console.

_**The runtime should be always Java 8 and use lodr-lambda-1.0.0.jar.  The jar file will be in the installation folder/lodr-lambda**_


Name | Handler | Memory | Timeout  
---- | ------- | ------ | ------
ProcessDataFunction | io.lodr.lambda.function.ProcessDataFunction | 1536 | 5 min
SampleSourceFunction | io.lodr.lambda.function.SampleSourceFunction | 512 | 5 min
SampleWranglerSourceFunction | io.lodr.lambda.function.SampleWranglerSourceFunction | 1536 | 5 min
ProcessDataInMemoryFunction | io.lodr.lambda.function.ProcessDataInMemoryFunction | 1536 | 5 min
CleanDataFunction | io.lodr.lambda.function.CleanDataFunction::cleanData | 192 | 5 min

After creating the CleanDataFunction in the AWS Lambda console, go to
_CloudWatch -> Events-Rules_ to create a rule as follows:

* **Schedule** - Fixed rate of 1 days
* **Type:** Lambda Function
* **Resource Name:** CleanDataFunction
* **Input:** Matched event

### Publishing Lambda Functions
You must attach the **Alias** _prod_ to $LATEST for each of the above functions.  

### Environment Variables for Lambda
Additionally, when we first created Lodr we relied on Access Keys and Access ID's to enable Lambda access to various resources.  The functions must be deployed with the access id and key of a user with read/write access to DynamoDB, CloudWatch, and the S3 upload bucket created above.

#### Environment Variables:
  - accessId
  - accessKey

#### Optional Env Variables (For renaming dynamo tables)
 - canceledQueryTbl
 - canceledJobTbl
 - columnInfoTbl
 - jobStatusTbl

## Deploying/Installing Lodr
Unzip lodr-on-prem.zip into your home directory.  If you are upgrading from a previous version, unzip into a separate directory and copy the “configs” directory from the old one to the new one.

**_NOTE: YAML files (file ending in .yml) are very sensitive to white spaces and tabs.  Use the spacebar to add spaces manually. Do not use tabs._**

### Lodr Mailer Configuration
Before we can launch Lodr we need to configure with your environment settings.  First we will setup how Lodr will send out emails:

**Edit lodr-on-prem/config/production.rb**

Update your email configuration setup here:

```ruby
 config.action_mailer.delivery_method = :smtp
 config.action_mailer.smtp_settings = {
    address:              'smtp.gmail.com',
    port:                 587,
    domain:               'lodr.io',
    user_name:            Rails.application.secrets.mail_user,
    password:             Rails.application.secrets.mail_password,
    authentication:       'plain',
    enable_starttls_auto: true  }
  config.action_mailer.default_url_options = { host: 'http://host_url' }
  config.action_mailer.asset_host = 'http://host_url'
```

Update the domain option set it correctly to domain of your Google Applications suite.  You should be able to configure these options to support any email service.

Finally, you need to update the default url and asset host.  This will affect links embedded in emails sent by Lodr.  This could be domain name you have, an ip address, or the ec2 instance name.  

For example, if you are running Lodr on port 3000 at mydomain.com then set the config as follows:
```ruby
  config.action_mailer.default_url_options = { host: 'http://mydomain.com:3000’ }
  config.action_mailer.asset_host = 'http://mydomain.com:3000’
```

### Lodr Database Configuration
This is the database that the Lodr Rails application uses to store application state.
**Edit lodr-on-prem/config/database.yml**

Enter your host, db name, username, and password for the production environment:
```yaml
production:
  <<: *default
  host: <db host>
  database: <db name>
  username: <user>
  password: <password>
```

### Lodr Web Server Configuration
When you run the **prepare** command, lodr will by default be configured to run behind an Nginx server which will proxy connections to a Ruby on Rails Puma application server over Unix socket.

### Lodr Environment Configuration
This is where the majority of the Lodr application can be configured.  **Edit config/secrets.yml** and modify the production section.

**_If this is your first time setting up Lodr, cd into the lodr directory and run `rake secret` to generate a secret key base._**

1. cd lodr
2. gem install bundler ( you might have to restart the shell )
3. bundle install
4. rake secret (copy this secret key)
5. cd ../ ( back to lodr-on-prem root )
6. **Edit config/secrets.yml** and enter s3, db, and the secret key from above.  This should be done for the production section only:

```yaml
production:
  secret_key_base: <key from above rake secret>
  s3_upload_bucket: <your bucket name>
  s3_key_id: <bucket access key id>
  s3_access_key: <bucket secret key>
  s3_bucket_region: us-west-2
  internal_webhook_base: http://localhost
  mail_user: email@gmail.com
  mail_password: gmail_pwd
  lodr_static_ips: ['35.163.61.96']
  job_manager: http://localhost:9000
```

6. If you change the port, be sure to update the port and domain of the Lodr host: internal_webhook_base
  - The Lodr job manager uses this information, to post back lambda processing status and other info.
7. After every code or config update run ./lodr.sh prepare
  - **You will have to run the prepare command twice most likely**
  - **The first will have migration failures**
8. Configure the job manager by editing config/application.conf.  Only the accessId, accessKey, region, and bucket are required.  Table and function names are configurable but optional.  All the relevant settings can be found at the bottom of the application.conf file

```config
  appMode=PROD
  accessId=<id>
  accessKey=<key>
  defaultRegion=us-west-2
  uploadBucket=lodr-user-uploads
  jobStatusTbl=JobStatus
  columnInfoTbl=ColumnInfoItem
  canceledJobTbl=CanceledJob
  canceledQueryTbl=CanceledQuery
  processDataFunction=ProcessDataFunction
  processDataInMemoryFunction=ProcessDataInMemoryFunction
  sampleSourceFunction=SampleSourceFunction
  sampleWranglerSourceFunction=SampleWranglerSourceFunction
```

## Running Lodr
When you start Lodr this will run the Puma web server, 4 instances of delayed_job service, and 1 instance of the lodr-job-manager.

If this is the first time, please run ./lodr.sh prepare after applying the configurations described above.
```
# Starting Lodr
./lodr.sh start
```
```
# Stopping Lodr
./lodr.sh stop
```

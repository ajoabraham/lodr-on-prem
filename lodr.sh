#!/bin/sh

clear
echo ""
echo "|-------------------------------|"
echo "|      WELCOME TO LODR.IO       |"
echo "|-------------------------------|"
echo ""

RED='\033[0;31m'
LIGHTRED='\033[1;31'
GREEN='\033[0;32'
YELLOW='\033[1;33'
CYAN='\033[0;36'
NC='\033[0m' # No Color

logs_path=`pwd`
LOGR="$logs_path/lodr/log/XXX.log {\n daily\n missingok\n rotate 7\n compress\n delaycompress\n notifempty\n copytruncate\n}"

test(){
  echo "${RED} PLEASE APPEND THE FOLLOWING TO /etc/logrotate.conf${NC}"
  echo $LOGR | tr -s XXX '*'
}

prepare(){
	echo "Hello! Preparing Lodr for Launch...."
	echo "Copying configuration files over"

	echo `cp -f config/secrets.yml lodr/config/secrets.yml`
	echo `cp -f config/database.yml lodr/config/database.yml`
	echo `cp -f config/puma.rb lodr/config/puma.rb`
	echo `cp -f config/production.rb lodr/config/environments/production.rb`
  echo `cp -f config/application.conf lodr-job-manager-1.0.0/conf/application.conf`

	echo "Done copying config files..."
	echo ""

	echo "Generating nginx conf file"
	rm -rf config/lodr-nginx.conf
	erb config/lodr-nginx.conf.erb > config/lodr-nginx.conf
	sudo rm /etc/nginx/nginx.conf
	sudo cp config/lodr-nginx.conf /etc/nginx/nginx.conf
	echo "Done creating nginx conf..."

	cd lodr
	bundle install

	echo "Precompiling assets"
	RAILS_ENV=production rake assets:precompile
	echo "done precompiling assets"

	echo "Running any migrations..."
	RAILS_ENV=production rake db:migrate
	echo "Done migrations"

  cd ../
  chmod a+x lodr-job-manager-1.0.0/bin/lodr-server

  # We need this to enable large payloads
  echo "Enabling ec2-user access to nginx folders"
  sudo chown -R ec2-user:ec2-user /var/lib/nginx

	echo "Setup log rotation"
	if [  -f /etc/logrotate.conf ]; then
		if grep -q lodr /etc/logrotate.conf; then
			echo "log rotation is already configured"
		else
      echo "___________________________"
      echo ""
      echo -e "${RED}PLEASE APPEND THE FOLLOWING TO /etc/logrotate.conf${NC}"
      echo "___________________________"
			echo -e $LOGR | tr -s XXX '*'
      echo "Then execute this command twice:"
      echo "sudo /usr/sbin/logrotate -f /etc/logrotate.conf"
		fi
	else
		echo "System does not have logrotate"
	fi
}

start(){
	echo "Starting up Lodr"
	echo "See README to change running port"

	if [  -f .pids ]; then
	    echo "lodr app already started... Run stop first"
		exit 1
	fi

	echo "Stopping nginx...."
	sudo /etc/init.d/nginx stop

  echo "Getting user upload bucket info..."
	S3KEYID=`ruby -ryaml -e "puts YAML::load(open(ARGV.first).read)['production']['s3_key_id']" config/secrets.yml`
  	S3KEY=`ruby -ryaml -e "puts YAML::load(open(ARGV.first).read)['production']['s3_access_key']" config/secrets.yml`
	UPLOADBUCKET=`ruby -ryaml -e "puts YAML::load(open(ARGV.first).read)['production']['s3_upload_bucket']" config/secrets.yml`
	S3REGION=`ruby -ryaml -e "puts YAML::load(open(ARGV.first).read)['production']['s3_bucket_region']" config/secrets.yml`

	mkdir .pids
	cd lodr
	RACK_ENV=production RAILS_ENV=production bundle exec pumactl -F config/puma.rb start &>/dev/null &
	echo "Started the lodr puma server"
	echo $! > ../.pids/lodr-app.pid
	echo "Successfully started the lodr app."

	echo "Starting nginx...."
	sudo /etc/init.d/nginx start

	echo "Starting delayed jobs..."
	RAILS_ENV=production PORT=$port bundle exec bin/delayed_job start -n 4
	echo "Successfully started delayed jobs."

	echo "Starting lodr job manager..."
	cd ../lodr-job-manager-1.0.0
	nohup bin/lodr-server -J-mx7168m -DappMode=prod -DdefaultRegion=$S3REGION -DuploadBucket=$UPLOADBUCKET -DaccessId=$S3KEYID -DaccessKey=$S3KEY &>/dev/null &
	echo $! > ../.pids/lodr-job-manager.pid
	echo "Successfully started the lodr job manager"
}

stop(){
	puma=.pids/lodr-app.pid
	if [  -f $puma ]; then
	  echo "Stopping lodr app"
		pid=`cat $puma`
		kill -9 $pid
	fi

	lbj=.pids/lodr-job-manager.pid
	if [  -f $lbj ]; then
	  echo "Stopping lodr job manager"
		pid=`cat $lbj`
		kill -9 $pid
	fi

	if [ -f lodr-job-manager-1.0.0/RUNNING_PID ]; then
		rm lodr-job-manager-1.0.0/RUNNING_PID
	fi

	rm -rf .pids
	echo "Stopping delayed job processes"
	cd lodr
	RAILS_ENV=production PORT=$port bundle exec bin/delayed_job stop -n 4

	echo "Lodr successfully stopped"
}

case $1 in
	"prepare")
		prepare ;;
	"start")  start ;;
    "stop")  stop ;;
	"test") test ;;
    *) usage ;;
esac

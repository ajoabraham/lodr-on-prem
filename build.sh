#!/bin/sh

clear

VERSION=`cat VERSION`

MAJOR=1
MINOR=1

IFS='.' read -r -a BUILD <<< "$VERSION";
BUILDI="${BUILD[2]}"
((BUILDI++))
VERSION=$MAJOR.$MINOR."$BUILDI"

echo "|-------------------------------|"
echo "|   BUILDING LODR.IO            |"
echo "    VERSION $VERSION            "
echo "|-------------------------------|"
echo ""

> VERSION
echo $VERSION > VERSION

cd ../
echo "Copying latest lodr master"
rm -rf lodr-on-prem/lodr/app
rm -rf lodr-on-prem/lodr/db
rm -rf lodr-on-prem/lodr/lib
rm -rf lodr-on-prem/lodr/test
rm -rf lodr-on-prem/lodr/public
rm -rf lodr-on-prem/lodr/vendor

cd lodr
git checkout master
cd ../
cp -R lodr/app lodr-on-prem/lodr/
cp -R lodr/db lodr-on-prem/lodr/
cp -R lodr/lib lodr-on-prem/lodr/
cp -R lodr/test lodr-on-prem/lodr/
cp -R lodr/public lodr-on-prem/lodr/
cp -R lodr/vendor lodr-on-prem/lodr/

> lodr-on-prem/lodr/app/views/shared/_drift.html.erb

echo "Creating lodr-on-prem-v$VERSION directory"
echo "  in " `pwd`
cp -R lodr-on-prem lodr-on-prem-v$VERSION
cd lodr-on-prem-v$VERSION

echo "Cleaning artificats"
rm -rf .git*
rm -rf .DS_Store
rm -rf .pids
rm build.sh

echo "Zipping up build..."
cd ../
zip -r -X lodr-on-prem-v$VERSION.zip lodr-on-prem-v$VERSION

echo ""
echo "|-------------------------------|"
echo "| FINISHED BUILDING LODR.IO     |"
echo "    VERSION $VERSION            "
echo "|-------------------------------|"
echo ""
echo "Remember config and gemfile are not copied. Manually update these."
